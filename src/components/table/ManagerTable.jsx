import React, { Component } from 'react'
import fire from 'C:/Users/ASUS/Desktop/JavaScript/Lessons/task5sketch/src/config/Fire.js';


const database = fire.database();
var ref = database.ref('drawings');
ref.on('value', gotData,errData);

var dataJSON = [];
var context;
function gotData(data){
    var drawings = data.val();
    if(drawings != null){
        var keys = Object.keys(drawings);
        for (var i = 0; i < keys.length; i ++){
            var k = keys[i];
            dataJSON[i] = {
                name: drawings[k].name,
                key: keys[i],
                url: drawings[k].url
            }
        }
    }
    context.setState(dataJSON);
    console.log("DB render");
}

function errData(err){
    console.log("Error!");
    console.log(err);

}

class ManagerTable extends Component {
    constructor(props) {
        super(props);
        dataJSON[0] = {
            name: '',
            key: '',
            url: ''
        }
        context = this;
        this.state = {
            data: dataJSON
        }
        this.handleButtonClick = this.handleButtonClick.bind(this);
    }

    handleButtonClick(e,row){
        console.log(row);
        this.props.updateState(row);
    }

    render() {
        console.log("Table render");
        return (
            <div>
            <table className="table table-striped table-bordered table-sm">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Users</th>
                        <th>Connection</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        dataJSON.map(row => (
                            <tr>
                                <td>{row.name}</td>
                                <td>0</td>
                                <td>
                                <button onClick={(e) => this.handleButtonClick(e, row)}>Connect</button>
                                </td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
            </div>
        ) 
    }
}

export default ManagerTable;