import firebase from 'firebase';
import 'firebase/storage';

const config = { /* COPY THE ACTUAL CONFIG FROM FIREBASE CONSOLE */
    apiKey: "AIzaSyChb68WWsI-w0DDOI1aaIoE898AigF68zA",
    authDomain: "meyertask2.firebaseapp.com",
    databaseURL: "https://meyertask2.firebaseio.com",
    projectId: "meyertask2",
    storageBucket: "meyertask2.appspot.com",
    messagingSenderId: "863111882301"
};
const fire = firebase.initializeApp(config);

export default fire;