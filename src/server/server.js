const express = require('express')
const http = require('http')
const socketIO = require('socket.io')

// our localhost port
const port = 4001

const app = express()

// our server instance
const server = http.createServer(app)

// This creates our socket using the instance of the server
const io = socketIO(server)

// This is what the socket.io syntax is like, we will work this later
io.sockets.on('connection', socket => {
    console.log('User connected  ' + socket.id)
    
    socket.on('join',(room) => {
        socket.join(room);
        console.log(room);
        socket.on('mouse', mouseMsg);
        socket.on('moveTo',(data) => {
            console.log("moveTO");
            console.log(data);
            socket.broadcast.to(room).emit('moveTo',data);
        })
    
        function mouseMsg(data) {
            socket.broadcast.to(room).emit('mouse',data);
        }
    })

    socket.on('disconnect', () => {
        console.log('user disconnected')
    })
})



server.listen(port, () => console.log(`Listening on port ${port}`))