import React, { Component } from 'react';
import Canvas from './Canvas';
import ManagerTable from './components/table/ManagerTable';
import Bootstrap from 'C:/Users/ASUS/Desktop/JavaScript/Lessons/task5sketch/node_modules/bootstrap/dist/css/bootstrap.css'
import fire from './config/Fire';
import './App.css';


class App extends Component {

    constructor(props) {
    super(props);
        this.state = ({
            isConnected: false,
            data : ''
        });
        this.updateState = this.updateState.bind(this);
    }

    updateState(row) {
        console.log("update state");
        this.setState({
            isConnected : !this.state.isConnected,
            data : row
        });
    }

    render() {
        console.log("app render");
        console.log(this.state.data);
        return (
        <div>
            <div>{this.state.isConnected ? ( <Canvas updateState ={this.updateState} data={this.state.data}/>) : (<ManagerTable updateState ={this.updateState}/>)}</div>
        </div>
        );
    }
}

export default App;
