import React, { Component } from 'react';
import fire from './config/Fire'
import socketIOClient from 'socket.io-client'
import './styles/main.scss';
import './App.css';

const database = fire.database();
var ref = database.ref('drawings');
var socket;
var ctxSocket, mouseMoveTo;
ref.on('value', gotData,errData);

var dbImages = new Map();
function gotData(data){
    var drawings = data.val();
    if(drawings != null){
        var keys = Object.keys(drawings);
        for (var i = 0; i < keys.length; i ++){
            var k = keys[i];
            var Data = {
                key: drawings[i],
                url: drawings[k].url
            }
            dbImages.set(drawings[k].name,Data);
        }
    }
}

function errData(err){
    console.log("Error!");
    console.log(err);

}
class Canvas extends Component {

    constructor(props) {
        super(props);
        this.state = {
            endpoint: "http://localhost:4001" // this is where we are connecting to with sockets
        }
        this.canvasMouseEnter = this.canvasMouseEnter.bind(this);      
        
    this.loadImage = this.loadImage.bind(this);  
    }


    componentWillMount(){
        this.setState({
            canvasSize: {canvasWidth: 800, canvasHeight: 600}
        })
    }

    componentDidMount() {
        const { canvasWidth, canvasHeight} = this.state.canvasSize;
        this.canvasHex.width = canvasWidth;
        this.canvasHex.height = canvasHeight;
        this.loadImage();
        var el = document.getElementById('mainCanvas');
        ctxSocket = el.getContext('2d')
        socket = socketIOClient(this.state.endpoint);
        var name = this.props.data.name;
        socket.emit('join',name);
        socket.on('moveTo',(data) =>{
            ctxSocket.lineWidth = 10;
            ctxSocket.lineJoin = ctxSocket.lineCap = 'round';
            ctxSocket.moveTo(data.x,data.y);
        })
        socket.on('mouse', (data) => {
            ctxSocket.lineTo(data.x, data.y);
            ctxSocket.stroke();
        });
    }

    loadImage() {
        var name = this.props.data.name;
        var el = document.getElementById('mainCanvas');
        var ctx = el.getContext('2d');
        const storage = fire.storage();
        var img = new Image();
        storage.ref('drawings/' + name).getDownloadURL().then(function(url) {
        img.onload = function() {
            ctx.drawImage(img,0,0);
        }
        img.src = url;
        img.crossOrigin = "Anonymous";
        }).catch(function(error) {
            console.log(error);
        });
    } 

    canvasMouseEnter() {
        var el = document.getElementById('mainCanvas');
        var ctx = el.getContext('2d');
        var name = this.props.data.name;
        var isDrawing;

        el.onmousedown = function(e) {
        isDrawing = true;
        ctx.lineWidth = 10;
        ctx.lineJoin = ctx.lineCap = 'round';
        ctx.moveTo(e.clientX, e.clientY);
        mouseMoveTo = {
            x: e.clientX,
            y: e.clientY
        }
        socket.emit('moveTo', mouseMoveTo);
        };
        el.onmousemove = function(e) {
        if (isDrawing) {
            var data = {
                x: e.clientX,
                y: e.clientY
            }
            ctx.lineTo(e.clientX, e.clientY);
            socket.emit('mouse', data);
            ctx.stroke();
        }
        };
        el.onmouseup = function() {
        isDrawing = false;
        var dataUrl = el.toDataURL("image/png;base64");
        var storage = fire.storage();
        var img = new Image();
        var base = dataUrl.slice(dataUrl.indexOf(',')+1);;
        img.src = dataUrl;
        storage.ref('drawings/' + name).putString(base,'base64');
        };
    }

    render() {
        return (
        <div>
            <canvas id="mainCanvas" ref={canvasHex => this.canvasHex = canvasHex} onMouseEnter={this.canvasMouseEnter}></canvas>
        </div>
        );
    }
}

export default Canvas;
